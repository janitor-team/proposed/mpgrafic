!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!   Copyright (C) 2008-2019 by Simon Prunet, Christophe Pichon et al     !
!   prunet iap.fr                                                        !
!                                                                        !
!   This program is free software; you can redistribute it and/or modify !
!   it under the terms of the GNU General Public License as published by !
!   the Free Software Foundation; either version 2 of the License, or    !
!   (at your option) any later version.                                  !
!                                                                        !
!   This program is distributed in the hope that it will be useful,      !
!   but WITHOUT ANY WARRANTY; without even the implied warranty of       !
!   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        !
!   GNU General Public License for more details.                         !
!                                                                        !
!   You should have received a copy of the GNU General Public License    !
!   along with this program; if not, write to the                        !
!   Free Software Foundation, Inc.,                                      !
!     59 Temple Place - Suite 330, Boston, MA  02111-1307, USA           !
!   or see https://www.gnu.org/licenses/licenses.html#GPL .              !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

module grafic_types
#ifdef HAVE_USE_MPI_F08
  use mpi_f08
#endif
  integer, parameter :: dp=selected_real_kind(12,200)
  integer, parameter :: sp=selected_real_kind(5,30)
  ! TODO: portability - "kind parameters are implementation dependent" -
  ! https://gcc.gnu.org/onlinedocs/gcc-6.3.0/gfortran/Old-style-kind-specifications.html
  ! possible solution - https://gcc.gnu.org/onlinedocs/gcc-6.3.0/gfortran/C_005fSIZEOF.html
  integer, parameter :: sp_bytes = 4

  integer, parameter :: i4b=selected_int_kind(9)
  integer, parameter :: i8b=selected_int_kind(18) !log(2*10^18)/log(2)=60.8
  integer, parameter :: dpc=kind((1.0_dp,1.0_dp))

  REAL(kind=dp), PARAMETER, public :: PI=3.141592653589793238462643383279502884197_dp

  ! FFTW stuff
  INTEGER,PARAMETER:: FFTW_FORWARD=-1, FFTW_BACKWARD=1

  INTEGER,PARAMETER:: FFTW_REAL_TO_COMPLEX=-1, FFTW_COMPLEX_TO_REAL=1

  INTEGER,PARAMETER:: FFTW_ESTIMATE=0, FFTW_MEASURE=1

  INTEGER,PARAMETER:: FFTW_OUT_OF_PLACE=0, FFTW_IN_PLACE=8

  INTEGER,PARAMETER:: FFTW_USE_WISDOM=16

  INTEGER,PARAMETER:: FFTW_THREADSAFE=128

  integer,parameter:: FFTW_TRANSPOSED_ORDER=1, FFTW_NORMAL_ORDER=0
  integer,parameter:: FFTW_SCRAMBLED_INPUT=8192
  integer,parameter:: FFTW_SCRAMBLED_OUTPUT=16384

#ifdef ADD1US
#define  rfftw3d_f77_create_plan  rfftw3d_f77_create_plan_
#define  rfftwnd_f77_destroy_plan rfftwnd_f77_destroy_plan_
#define  rfftwnd_f77_one_real_to_complex rfftwnd_f77_one_real_to_complex_
#define  rfftw3d_f77_mpi_create_plan  rfftw3d_f77_mpi_create_plan_
#define  rfftwnd_f77_mpi_destroy_plan rfftwnd_f77_mpi_destroy_plan_
#define  rfftwnd_f77_mpi rfftwnd_f77_mpi_
#define  rfftwnd_f77_mpi_local_sizes rfftwnd_f77_mpi_local_sizes_
#endif

#ifndef HAVE_USE_MPI_F08
  INCLUDE 'mpif.h'
#endif

end module grafic_types

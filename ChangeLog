2020-08-01 boud
	* v0.3.19 - Fix Debian bug #957565 related to gcc-10 being
	more strict about fortran overloading of procedure modules
	compared to gcc-9. The newer gcc does not accept the possibility
	that a compiled routine might never be used as a result
	of configure.ac and src/Makefile.am decisions.

	Since mpgrafic uses an MPI library, any regression test has to
	check the MPI environment. The existing regression test has
	mainly only been tested on Debian machines. With this update,
	if the environment sets the environment variable HAVE_OPENMPI to
	the one-character string `1`, then this new version
	of the test, `regression-test-0.3.7.9.sh`, should
	run the test assuming that openmpi is available. Any other cases
	should run as before.

2019-01-17 boud
	* v0.3.18 - Shift ../config.h higher in src/parallel_io.c .
	This should make the i386 build LFS safe, but remains to be
	tested on a real or virtual i386 machine.

2019-01-17 boud
	* v0.3.18 - Shift ../config.h higher in src/parallel_io.c .
	This should make the i386 build LFS safe, but remains to be
	tested on a real or virtual i386 machine.

2019-01-16 boud
	* v0.3.17 - Allow optionally using an input file named on the
	command line to replace reading from standard input. This brings
	mpgrafic more into line with what users might expect, and bypasses
	the openmpi/hurd bug, in which openmpi cannot handle reading from
	standard input: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=919335 .
	The regression test has been shifted to using the filename as a file
	instead of as standard input, so that automated testing will not
	block the installation of mpgrafic on Debian GNU/Hurd.

2018-12-23 boud
	* v0.3.16 - minor portability and documentation fixes

	* src/grafic1.inc, src/mpgrafic.1, src/mpgrafic.f90: rm confusing
	word 'mesh'. The word 'mesh' in the context of cosmological structure
	formation simulations risks confusion - particle and mesh
	simulations are fundamentally different techniques, even though
	they're often hybridised together, so 'mesh' is removed in this
	commit (only present in one obsolete comment and one non-obsolete
	comment). A bit more explanation of what "grid" means has been
	added.

        * m4/ax_mpi.m4: the 2.0.2-2 version of openmpi doesn't seem to have
	a symlink /usr/bin/mpicc pointing to mpicc.openmpi . So a safe
	hack is to add mpicc.openmpi as an alternative binary.

        * autotools: shift AX_MPI to a subdirectory m4/ to follow modern
	conventions: As explained in guides such as
	http://freesoftwaremagazine.com/articles/brief_introduction_to_gnu_autotools/ ,
	the preferred method of including local additions of m4 scripts
	is to put them in a subdirectory called m4/ .

        * src/parallel_io.c: portable LFS support (offsets in pread, pwrite) should
	be handled by the autotools build system using `config.h`, which wasn't
	previously included in `parallel_io.c`.

	* src/rfftw3d_f77_mpi_create_plan_c_wrap - add missing prototype
        for rfftw3d_mpi_create_plan_c_wrap()

2017-08-18 boud
	* v0.3.15 - src/wrap_rfftw3d_mod.f90 - use mpi type only if
	use mpi_f08 is enabled

	* v0.3.14 - grafic_io.f90: length now declared as C type (c_size_t)
	since size_t can be 32-bit even on 64-bit machines;
        mpi_f08 preprocessor macro test added for "use mpi_f08" in .f90 files,
	with fallback to "include 'mpif.h'"

	* v0.3.13 - regression-test-0.3.7.9.sh: use
	MPI_ALLOW_RUN_AS_ROOT for the openmpi option --allow-run-as-root
	if this is needed for automated testing.

2017-08-14 boud
	* v0.3.12 - parallel_io.c: bug fix: do not write null to
	unallocated location (fortran vs C string conversion);
	update regression-test-0.3.7.9.sh so that the MCA plm_rsh_agent
	is /bin/false. Upgrading mpif.h to "use mpi_f08" in multlipe
	files, along with int64_t replacing C long and
	downgrading  MaxRandNumStreams to int32_t.

2017-07-30 boud
	* v0.3.11 - LFS: AC_SYS_LARGEFILE added to configure.ac, but int64_t
	retained in parallel_io.c rather than replacing it by off_t,
	because ISO C binding (c_off_t) does not seem to be standard;
	minor tidying in response to some compiler warnings;
	--enable-devmode option during ./configure.

2017-01-28 boud
	* v0.3.10 - portability: bypass fortran->C communicator types
	Debian bug #851918 on the s390x architecture, by adding and using
	two wrapper files: src/rfftw3d_mpi_create_plan_c_wrap.c,
	src/wrap_rfftw3d_mod.f90

2017-01-22 boud
	* v0.3.8 - portability: reproducible lower precision printed floats;
	iso_C_binding; avoid implementation-dependent integer sizes;
	avoid implementation-dependent fortran kind values
	* v0.3.9 - portability: lower precision printed float mppower.f90;
	partial code tidying based on strict compiler warnings;
	iseed, seeds -> c_long -> up to 4e9 cores can run with different seeds

2017-01-20 boud
	* v0.3.7.7 - regression test 0.3.7.7 - reduce required precision;
	avoid implementation-dependent assumption on `kind' parameter value
	* v0.3.7.8 - as for 0.3.7.7 but includes regression-test-0.3.7.8.sh
	* v0.3.7.9 - regression test 0.3.7.9 - reduce required precision

2017-01-19 boud
	* v0.3.7.6 - regression test - remove deprecated openmpi/orte option

2017-01-16 boud
	* v0.3.7.2 - make check - avoid running as root if possible
	* v0.3.7.3 - MPI_ALLOW_RUN_AS_ROOT - allow running make check as root
	* v0.3.7.4 -  --allow-run-as-root is specific to openmpi
	* v0.3.7.5 revert allow-run-as-root related changes
	- see thread: https://lists.debian.org/debian-mentors/2017/01/msg00322.html;
	remove top Makefile.am `foreign' and `1.4' options

2017-01-15 boud
	* v0.3.6.4 - Regression test edit descriptors for reproducible floats
	* v0.3.7 - rename 0.3.6.4 since it passes standard tests
	* v0.3.7.1 - configure.ac: prioritise mpifort over deprecated (jessie
	or earlier) mpif90

2017-01-14 boud
	* v0.3.6.2 - System and mpi-implementation dependence of code regression test
	* v0.3.6.3 - regression test: s/return/exit/g

2017-01-13 boud
	* v0.3.6 - Simpler src/Makefile.am, .in, rm superfluous ltmain.sh
	* v0.3.6.1 - Avoid regression test dependence on mktemp

2017-01-12 boud
	* v0.3.5 - Regression test - fast full run should match output for version 0.3.4

2017-01-05 boud
	* v0.3.4 - replace pause statements - obsolete since f90; whitespace changes
	* v0.3.3 - minor changes motivated by debianisation

2016-12-27 boud
	* v0.3.2.1 - first debianisable version (tag v0.3.2 wrongly pushed)
        * particle resolution: allow user to avoid recompiling for simple usage
        * +minimal man page mpgrafic.1
        * minimal GNU-style long and short options; hardwired in mpgrafic.f90
        * autotools updates following: autoreconf -i -f
        * add examples: Input.stdin, Output.stdout; for portability: compile
        * tidying for debian version; rm kdevelop files obsolete, not essential
        * +config.h.in - configure will not work without this

2016-12-25 boud
	* v0.3.1 - first version on bitbucket

2016-12-24 boud + simon + christophe
	* configure.ac: handle both fftw and [sd]fftw library names of fftw2
	* parts of v0.3: +templates/gdl
	* parts of v0.3: +gdl/idl routines, including licence GPLv2
	* AUTHORS, README, insert GPLv2 licence in src/*
	* c_gsl_wrap.c - without including gsl_randist.h, results are nonsense
	* use GSL gaussian ziggurat directly - simpler random solution
	* mpgrafic.f90: clarify that ramses comment is for very old ramses only
	* parts of v0.3: Simon changes + s/non-free-rng/gsl_rng/g
	* parts of v0.3: overflow correction nx*ny*nz; random num generator; not yet compilable

2016-11-12 boud
	* v0.2.1.1 +grafic1.inc in EXTRA_DIST
	* v0.2.1 - minor changes - may not work for everyone
	* mv configure.in configure.ac
	* 1D number of particles: FCFLAGS=-DMPGRAFIC=32 ./configure
	* configure.ac for debian/jessie; autoconf && ./configure && make

2008-05-01 simon prunet + christophe pichon
	* v0.2
	* md2sum333e8eff20312a8a8227b0692dd547bc  mpgrafic-0.2.tar.gz
	* URL: ftp://ftp.iap.fr/pub/from_users/prunet/mpgrafic-0.2.tar.gz

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   Copyright (C) 2008-2016 by Simon Prunet                               ;
;   prunet iap.fr                                                         ;
;                                                                         ;
;   This program is free software; you can redistribute it and/or modify  ;
;   it under the terms of the GNU General Public License as published by  ;
;   the Free Software Foundation; either version 2 of the License, or     ;
;   (at your option) any later version.                                   ;
;                                                                         ;
;   This program is distributed in the hope that it will be useful,       ;
;   but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
;   GNU General Public License for more details.                          ;
;                                                                         ;
;   You should have received a copy of the GNU General Public License     ;
;   along with this program; if not, write to the                         ;
;   Free Software Foundation, Inc.,                                       ;
;   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              ;
;   or see https://www.gnu.org/licenses/licenses.html#GPL .               ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro read_grafic, filename, nn, cosmo, cube, kfile=kfile

if (n_params() ne 4) then begin
    print, 'Usage: read_grafic, filename, nn, cosmo, cube'
    return
endif

openr,1,filename,/f77

nn=lonarr(3)
cosmo=fltarr(8)

readu,1,nn,cosmo
if (keyword_set(kfile)) then begin
	n20 = 2*(nn(0)/2+1)
	tempo = fltarr(n20,nn(1))
	cube= fltarr(n20,nn(1),nn(2))
endif else begin	
	tempo = fltarr(nn(0),nn(1))	
	cube = fltarr(nn(0),nn(1),nn(2))
endelse

for i=0,nn(2)-1 do begin

    readu,1,tempo
    cube(*,*,i) = tempo

endfor
close,1
end
